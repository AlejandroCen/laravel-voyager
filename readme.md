# LARAVEL-VOYAGER

Para un correcto funcionamiento del proyecto se requiere de PHP, Composer y MariaDB, es necesario tener las siguientes consideraciones en cuanto al entorno de trabajo.

## Requerimientos
* PHP >= 7.1.3 
* Composer >= 1.8.0
* MariaDB >= 10.3.11

## Configuración

Clonar el proyecto desde el repositorio.
```
$ git clone https://<username>@bitbucket.org/AlejandroCen/laravel-voyager.git
```

Crear un archivo llamado *".env"* en la raíz del proyecto, tomar de ejemplo el archivo *".env.example"*.

Crear e importar la base de datos que se encuenta en *"Downloads"* posteriormente configurar el archivo *".env"*.

Instalar las dependencias del proyecto.
```
$ composer install
```

Generar la *"key"* de Laravel.
```
$ php artisan key:generate 
```

Crear un usuario para el administrador.
```
$ php artisan voyager:admin your@email.com --create
```

## Iniciar el servidor de desarrollo

Para iniciar el servidor de desarrollo en el navegador se ejecuta el siguiente comando.
```
$ php artisan serve
```

*URL para acceder al administrador: http://127.0.0.1:8000/admin*
